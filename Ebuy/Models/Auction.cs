﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ebuy.Models
{
    public class Auction
    {
        public long Id { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "ннадо < 50")]
        public string Title { get; set; }
        
        [Required]
        public string Description { get; set; }
        
        [Range (1, 10000, ErrorMessage = "попади в диапазон 1-10000 !!!")]
        public decimal StartPrice { get; set; }
        public decimal CurrentPrice { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}